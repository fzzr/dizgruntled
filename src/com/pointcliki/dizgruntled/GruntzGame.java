package com.pointcliki.dizgruntled;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.EntityManager;
import com.pointcliki.core.Manager;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.dizgruntled.editor.EditorScene;
import com.pointcliki.dizgruntled.rez.MonolithFile;
import com.pointcliki.input.InputManager;

public class GruntzGame extends PointClikiGame {
	
	private static StartScene fStartScene;
	private final static String TITLE = "Dizgruntled Alpha 2.7";
	
	/**
	 * The games main method that is run when the game begins
	 * 
	 * @param args Optional arguments to pass dizGruntled
	 */

	public static void main(String[] args) {		
		try {
			
			System.out.println("*************************");
			System.out.println("* Dizgruntled Alpha 2.7 *");
			System.out.println("*************************");
			
			// Make UI look like the system
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			
			// Add native path
			addLibraryPath("lib/natives/");
				
			configureGame();
			configureApp();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void configureGame() {
		sGame = new GruntzGame();
	}

	private boolean fMute = false;
	private boolean fFullscreen = true;
	private Vector2f fScreenSize = new Vector2f(1024, 768);
		
	/**
	 * Create a new StateBasedGame
	 * @throws GruntException 
	 */
	public GruntzGame() {
		// Call super
		super(TITLE);
	}
	
	public static GruntzGame instance() {
		return (GruntzGame) PointClikiGame.instance();
	}
	
	public static void configureApp() {
		try {
			sApp = new AppGameContainer(sGame);
			sApp.setIcon("lib/icon.png");
			
			try {
				sApp.setDisplayMode(451, 600, false);
			} catch (SlickException e1) {
				System.err.println("Can't initialize the display");
				/*
				try {
					sApp.setDisplayMode(fStart.screenX(), fStart.screenY(), false);
				} catch (SlickException e) {
					System.err.println("Can't initialize the display");
					System.exit(0);
				}
				*/
			}
			/*
			if (fStart.windowless()) System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
			*/
			sApp.setVerbose(false);
			sApp.setShowFPS(false);
			sApp.setVSync(false);
			try {
				sApp.start();
			} catch (SlickException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	public static GruntzResourceManager resourceManager() {
		return sGame.manager(GruntzResourceManager.class);
	}
	
	/**
	 * Override this method to add your own managers to the game.
	 */
	@Override
	public void configureManagers() {
		if (fManagers != null) return;
				
		// Create the managers
		fResourceManager = new GruntzResourceManager();
		fInputManager = new InputManager();
		fEntityManager = new EntityManager();
		
		// Add to the manager set
		fManagers = new HashMap<Class<? extends Manager>, Manager>();
		fManagers.put(GruntzResourceManager.class, fResourceManager);
		fManagers.put(InputManager.class, fInputManager);
		fManagers.put(EntityManager.class, fEntityManager);
		
		resourceManager().textString(0);
	}

	@Override
	protected void configureScenes() {
		startMenu();
	}
	
	public void startEditor() {
		
		boolean okay = false;
		try {
			sApp.setDisplayMode((int) fScreenSize.x, (int) fScreenSize.y, false);
			okay = true;
		} catch (SlickException e) {
			JFrame frame = new JFrame();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setAlwaysOnTop(true);
			JOptionPane.showMessageDialog(frame, "Sorry, Dizgruntled can't use this screen resolution. Please choose another", "Screen Resolution Problem", 0);
			frame.dispose();
		}
		if (okay) {
			fStartScene.cleanup();
			fScenes.clear();
			resourceManager().initLevel();
			EditorScene editor = new EditorScene(this);
			fScenes.add(editor);
			setCurrentScene(editor);
			editor.init();
		}
	}

	public void playLevel(File file) {
		boolean okay = false;
		try {
			sApp.setDisplayMode((int) fScreenSize.x, (int) fScreenSize.y, fFullscreen);
			okay = true;
		} catch (SlickException e) {
			JFrame frame = new JFrame();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setAlwaysOnTop(true);
			JOptionPane.showMessageDialog(frame, "Sorry, Dizgruntled can't use this screen resolution. Please choose another", "Screen Resolution Problem", 0);
			frame.dispose();
		}
		if (okay) {
			fCurrentScene.cleanup();
			fScenes.clear();
			resourceManager().initLevel();
			fCurrentScene = new LevelScene(this, file);
			fScenes.add(fCurrentScene);
			fCurrentScene.init();
		}
	}
	
	public void playLevel(MonolithFile wwd) {
		boolean okay = false;
		try {
			sApp.setDisplayMode((int) fScreenSize.x, (int) fScreenSize.y, fFullscreen);
			okay = true;
		} catch (SlickException e) {
			JFrame frame = new JFrame();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setAlwaysOnTop(true);
			JOptionPane.showMessageDialog(frame, "Sorry, Dizgruntled can't use this screen resolution. Please choose another", "Screen Resolution Problem", 0);
			frame.dispose();
		}
		if (okay) {
			fCurrentScene.cleanup();
			fScenes.clear();
			resourceManager().initLevel();
			fCurrentScene = new LevelScene(this, wwd);
			fScenes.add(fCurrentScene);
			fCurrentScene.init();
		}
	}

	public void startMenu() {
		try {
			sApp.setDisplayMode(451, 600, false);
		} catch (SlickException e) {}
		if (fCurrentScene != null) fCurrentScene.cleanup();
		fScenes.clear();
		fStartScene = new StartScene(this);
		setCurrentScene(fStartScene);
		fScenes.add(fStartScene);
		fStartScene.init();
	}

	public boolean mute() {
		return fMute;
	}
	public void mute(boolean b) {
		fMute = b;
	}
	
	public boolean fullscreen() {
		return fFullscreen;
	}
	public void fullscreen(boolean b) {
		fFullscreen = b;
	}
	
	public Vector2f screenSize() {
		return fScreenSize;
	}
	public void screenSize(Vector2f size) {
		fScreenSize = size;
	}
	
	/**
	* Adds the specified path to the java library path
	*
	* @param pathToAdd the path to add
	* @throws Exception
	*/
	public static void addLibraryPath(String pathToAdd) throws Exception{
	    final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
	    usrPathsField.setAccessible(true);
	 
	    //get array of paths
	    final String[] paths = (String[])usrPathsField.get(null);
	 
	    //check if the path to add is already present
	    for(String path : paths) {
	        if(path.equals(pathToAdd)) {
	            return;
	        }
	    }
	 
	    //add the new path
	    final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
	    newPaths[newPaths.length-1] = pathToAdd;
	    usrPathsField.set(null, newPaths);
	}
}
package com.pointcliki.dizgruntled.player;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.FrameManager;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.core.EntityQuery;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.logic.Grunt;

public class HumanPlayer extends GruntzPlayer {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3044509139142991290L;
	
	protected EntityQuery<Grunt> fSelected;

	private boolean fWon;

	public HumanPlayer(String name) {
		super(name);
		fSelected = new EntityQuery<Grunt>();
	}
	
	public void selectGrunt(Grunt g) {
		if (!PointClikiGame.inputManager().isCtrlPressed()) for (Grunt g2: fSelected) if (!g2.equals(g)) deselectGrunt(g2);
		if (g.selected()) return;
		fSelected.add(g);
	}
	
	public void deselectGrunt(Grunt g) {
		g.deselect();
		fSelected.remove(g);
	}
	
	public void click(GridCoordinate tile, int button) {
		if (button == 1) {
			issueMove(tile);
		} else if (button == 0) {
			issueAction(tile);			
		}
	}
	
	@Override
	public void win() {
		fWon = true;
		for (Grunt g: fUnits) g.win();
		playerManager().parent().manager(FrameManager.class).queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/EXITZ/DISAPPEAR");
				return Minion.FINISH;
			}
		}, 120);
		playerManager().parent().manager(FrameManager.class).queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				levelScene().hud().showText("You Won!");
				return Minion.FINISH;
			}
		}, 200);		
	}

	public void issueMove(GridCoordinate xy) {
		if (fSelected.size() == 0) return;
		for (Grunt g: fSelected) g.move(xy);
		
		// Target cursor
		//fSelected.entity().scene(LevelScene.class).mapManager().map().sfx(0, "GAME/ANIZ/TARGETCURSOR", "GAME/IMAGEZ/LIGHTING/TARGETCURSOR", null, xy);
		/*
		CommandMessage m = new CommandMessage(OldTimeManager.getCurrentFrame() + 10, myState.selectedGrunts.get(0), GruntCommand.Move, xy);
		
		PlayerManager.queueCommand(m);
		
		if (NetworkManager.getLocalPlayerID() == 1) {
			NetworkManager.messagesOut.add(m);
		}
		*/
	}
	
	public void issueAction(GridCoordinate xy) {
		if (fSelected.size() == 1) {
			fSelected.entity().useTool(xy);
		}
	}

	public boolean won() {
		return fWon;
	}
}
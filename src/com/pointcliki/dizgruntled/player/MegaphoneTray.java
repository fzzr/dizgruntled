package com.pointcliki.dizgruntled.player;

import java.util.HashMap;

public class MegaphoneTray {
	
	private HashMap<Integer, String> fTypes;
	private HashMap<Integer, String> fItems;
	private int fCurrentIndex = 0;
	
	public MegaphoneTray() {
		
		fTypes = new HashMap<Integer, String>();
		fItems = new HashMap<Integer, String>();
		
	}
	
	public boolean empty() {
		
		return !fTypes.containsKey(fCurrentIndex);
		
	}
	
	public void set(int order, String type, String item) {
		
		fTypes.put(order, type);
		fItems.put(order, item);
		
	}
	
	public void consume() {
		fCurrentIndex++;
	}
	
	public String type(int order) {
		
		return fTypes.get(order);
		
	}
	
	public String type() {
		
		return fTypes.get(fCurrentIndex);
		
	}
	
	public String item(int order) {
		
		return fItems.get(order);
		
	}
	
	public String item() {
		
		return fItems.get(fCurrentIndex);
		
	}

}

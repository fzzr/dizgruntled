package com.pointcliki.dizgruntled.map;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;
import com.pointcliki.dizgruntled.rez.MonolithFile;
import com.pointcliki.dizgruntled.rez.MonolithWWD;

public class GruntzMapManager extends Manager {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -2545534706116391193L;
	
	protected Map fMap;
	protected File fLocation;

	public GruntzMapManager(IManagerGroup parent) {
		super(parent);
	}
	
	public Map map() {
		return fMap;
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		return null;
	}

	@Override
	public void restore(Manager from) {
	}

	@Override
	public void cleanup() {
	}

	public boolean save() {
		if (fLocation == null || fLocation.getName().toLowerCase().endsWith(".wwd")) {
			return saveAs();
		}
		try {
			JSONObject o = fMap.exportToJSON();
			FileWriter writer = new FileWriter(fLocation);
			o.write(writer);
			writer.close();
		} catch (JSONException e) {
			System.err.println("Couldn't save the map");
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println("Couldn't save the map");
			System.err.println(e.getMessage());
		}
		return true;
	}
	
	public File file() {
		return fLocation;
	}
	
	public boolean saveAs() {
		JFileChooser saver = new JFileChooser();
		saver.addChoosableFileFilter(new FileNameExtensionFilter("Dizgruntled Maps (*.map)", "map"));
		saver.setDialogTitle("Save Dizgruntled Map");
		int r = saver.showSaveDialog(null);
		if (r != JFileChooser.APPROVE_OPTION) return false;
		
		File f = saver.getSelectedFile();
		String name = f.getName();
		if (!name.toLowerCase().endsWith(".map") && !name.contains(".")) f = new File(f.getPath() + ".map");		
		if (f.exists()) {
			JFrame frame = new JFrame();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setAlwaysOnTop(true);
			int s = JOptionPane.showConfirmDialog(frame, "Are you sure you want to overwrite " + f.getName() + "?", "Overwrite File", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			frame.dispose();
			if (s == 1) return true;
		}
		fLocation = f;
		save();
		return true;
	}
	
	public void setLocation(File f) {
		fLocation = f;
	}
	
	public void load(File f) {
		try {
			if (f.getName().toLowerCase().endsWith(".wwd")) {
				fMap = new Map(this, new MonolithWWD(new MonolithFile(f)));
			} else {
				JSONObject json = new JSONObject(new JSONTokener(new FileReader(f)));
				fMap = new Map(this, json);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't load the map");
			System.err.println(e.getMessage());
		} catch (JSONException e) {
			System.err.println("Couldn't load the map");
			System.err.println(e.getMessage());
		}
		fLocation = f;
	}
	
	public void importLevel(MonolithFile f) {
		fMap = new Map(this, new MonolithWWD(f));
		fLocation = null;
	}

	public void newLevel(String world, int x, int y) {
		fMap = new Map(this, world, x, y);
		fLocation = null;
	}
}

package com.pointcliki.dizgruntled.map;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;
import com.pointcliki.dizgruntled.LevelScene;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.UIEntity;

public class LogicLayer extends UIEntity {
	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1L;
	
	private MapEntity fMapEntity;
	
	public LogicLayer(MapEntity me) {
		fMapEntity = me;
	}
	
	@Override
	public void renderEntities(Graphics graphics, long currentTime) {
		// Render the entitys
		for (Entity entity: this) {
			// Cache position in case it moves
			int x = (int) entity.position().getX();
			int y = (int) entity.position().getY();
			
			// Ensure clipped
			if (x + entity.span().getMaxX() < -fPosition.x || y + entity.span().getMaxY() < -fPosition.y) continue;
			if (x + entity.span().getX() > fMapEntity.span().getWidth() - fPosition.x) continue;
			if (y + entity.span().getY() > fMapEntity.span().getHeight() - fPosition.y) continue;
			// Transform the graphics
			try {
				graphics.translate(x, y);
				entity.render(graphics, currentTime);
			} finally {
				// Transform the graphics back
				graphics.translate(-x, -y);
			}
		}
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		super.handleUIMouseEvent(type, local, event);
		// Move
		if (type.equals("mouse.down") && !fMapEntity.map().editing()) {
			scene(LevelScene.class).human().click(new GridCoordinate((int) (local.x / 32f), (int) (local.y / 32f)), event.button());
		}
		if (fMapEntity.map().editing() && type.equals("mouse.down") && event.button() == 1) for (Logic l: fMapEntity.map().selectedLogics()) fMapEntity.map().deselectLogic(l);
	}
}
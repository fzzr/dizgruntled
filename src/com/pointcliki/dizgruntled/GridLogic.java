package com.pointcliki.dizgruntled;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.grid.GridManager;
import com.pointcliki.grid.IGridEntity;

/**
 * This interface describes an in-game logic that is aligned to the grid
 * 
 * @author Hugheth
 * @since alpha 2.5
 */
public abstract class GridLogic extends Logic implements IGridEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -6041735096435980806L;
	
	protected GridCoordinate fTile;
	
	/**
	 * Initialize the logic once it has been added to a scene
	 */
	public void init(Map map) {
		super.init(map);
		calculateTile();
		gridManager().addObject(fTile, this);
	}
	
	protected void calculateTile() {
		fTile = new GridCoordinate((int) Math.floor((fPosition.x - 16f) / 32f), (int) Math.floor((fPosition.y - 16f) / 32f));
	}
	
	@Override
	public GridManager gridManager() {
		return manager(GridManager.class);
	}
	
	@Override
	public void cleanup() {
		if (fScene != null) gridManager().removeObject(fTile, this);
		super.cleanup();
	}
	
	@Override
	public void setTile(GridCoordinate tile) {
		gridManager().moveObject(fTile, tile, this);
		fTile = tile;
		position(new Vector2f(tile.x() * 32f + 16f, tile.y() * 32f + 16f));
	}
	
	@Override
	public GridCoordinate getTile() {
		return fTile;
	}
}

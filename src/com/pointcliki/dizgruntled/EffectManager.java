package com.pointcliki.dizgruntled;

import java.util.HashMap;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;

public class EffectManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5061366204906699551L;
	
	private TriggerDispatcher fDispatcher;
	private HashMap<String, Integer> fTriggerCount;
	private HashMap<String, Boolean> fCheckToggle;
	private HashMap<String, Integer> fTimeDelays;

	public EffectManager(IManagerGroup g) {
		super(g);
		fDispatcher = new TriggerDispatcher();
		fTriggerCount = new HashMap<String, Integer>();
		fCheckToggle = new HashMap<String, Boolean>();
		fTimeDelays = new HashMap<String, Integer>();
	}
	public void setCheckpointGroup(String group) {
		fCheckToggle.put(group, false);
	}
	
	public void contributeTimeDelay(String group, int delay) {
		if (fTimeDelays.containsKey(group)) {
			fTimeDelays.put(group, Math.max(delay, fTimeDelays.get(group)));
		} else {
			fTimeDelays.put(group, delay);
		}
	}
	
	public int timeDelayForGroup(String group) {
		if (!fTimeDelays.containsKey(group)) return -1;
		return fTimeDelays.get(group);
	}
	
	public boolean checkpointReached(String group) {
		if (fCheckToggle.containsKey(group))
			return fCheckToggle.get(group);
		
		return false;
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
	public void checkpoint(String group) {
		if (fCheckToggle.containsKey(group)) fCheckToggle.put(group, true);
	}

	public void triggerOn(String group) {
		if (!fTriggerCount.containsKey(group)) fTriggerCount.put(group, 0);
		fTriggerCount.put(group, fTriggerCount.get(group) + 1);
		fDispatcher.trigger(group, true);
	}
	
	public void triggerOff(String group) {
		if (!fTriggerCount.containsKey(group)) fTriggerCount.put(group, 0);
		fTriggerCount.put(group, fTriggerCount.get(group) - 1);
		fDispatcher.trigger(group, false);
	}
	
	public Dispatcher<TriggerEvent> dispatcher() {
		return fDispatcher;
	}
	
	
	private class TriggerDispatcher extends Dispatcher<TriggerEvent> {

		/**
		 * Serial key
		 */
		private static final long serialVersionUID = 7695353365548566630L;
		
		protected void trigger(String group, boolean on) {
			dispatchEvent(group, new TriggerEvent(EffectManager.this.fTriggerCount.get(group), on));
		}
	}
	
	public static class TriggerEvent implements IEvent {
		
		private int fCount;
		private boolean fOn;
		
		public TriggerEvent(int count, boolean on) {
			fCount = count;
			fOn = on;
		}
		
		public boolean on() {
			return fOn;
		}
		
		public int count() {
			return fCount;
		}
	}
}
package com.pointcliki.dizgruntled;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithWWD;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.input.MouseEvent;

/**
 * This interface describes an in-game logic that does not display
 * to the player during the game.
 * 
 * @author Hugheth
 * @since alpha 2.5
 */
public abstract class InvisibleLogic extends GridLogic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -6041735096435980806L;
	
	protected Vector2f fStart;
	protected int fFixed = 0;
	protected boolean fSelected;
	protected boolean fSnapToGrid = false;
	
	@Override
	protected void calculateTile() {
		fTile = new GridCoordinate((int) Math.floor((fPosition.x + 1) / 32f), (int) Math.floor((fPosition.y + 1) / 32f));
	}

	/**
	 * Import the logic from a WWD file
	 * @param logic The type of logic i.e. EyeCandy
	 * @param file The image file i.e. GAME/IMAGEZ/WAPWORLDONLY/TRIGGER
	 * @param animation The optional animation file i.e. GAME/ANIZ/WATER1.ANI, null otherwise
	 * @param data The byte data following these, containing e.g. smarts, health, rects
	 */
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		fFixed = 10000;
		fSpan = new Rectangle(0, 0, 17, 17);
		Vector2f pos = MonolithWWD.readPosition(data);
		position(new Vector2f((int) Math.floor((pos.x) / 32f) * 32f - 1, (int) Math.floor((pos.y) / 32f) * 32f - 1));
		order(10000);
	}
	
	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		fFixed = 10000;
		fSpan = new Rectangle(0, 0, 17, 17);
		String[] str = object.optString("xy").split(" ");
		Vector2f pos = new Vector2f(Float.valueOf(str[0]), Float.valueOf(str[1]));
		position(new Vector2f((int) Math.floor((pos.x) / 32f) * 32f - 1, (int) Math.floor((pos.y) / 32f) * 32f - 1));
		order(10000);
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject obj = super.exportToJSON();
		obj.put("xy", (int)Math.floor(fPosition.x + 1) + " " + (int)Math.floor(fPosition.y + 1));
		return obj;
	}
	
	@Override
	public void init(Map map) {
		super.init(map);
		if (map.editing()) {
			List<InvisibleLogic> ls = gridManager().getEntitiesOfTypeAt(fTile, InvisibleLogic.class);
			if (ls.size() > 1) {
				if (ls.size() == 2) fPosition.x += 16f;
				else if (ls.size() == 3) fPosition.y += 16f;
				else if (ls.size() == 4) {
					fPosition.x += 16f;
					fPosition.y += 16f;
				} else {
					fPosition.x += Math.random() * 16f;
					fPosition.y += Math.random() * 16f;
				}
			}
		} else {
			for (Entity e: this) removeChild(e);
		}
		opacity(0.8f);
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		if (fOpacity < 0.5f) return;
		fDispatcher.dispatchEvent(type, event);
		if (type.equals("mouse.down")) {
			fStart = local;
			select();
		}
		if (!fMap.editing()) return;
		else if (type.equals("mouse.drag")) {
			Vector2f change = local.sub(fStart);
			if (fSnapToGrid && !PointClikiGame.inputManager().isAltPressed())
				change = new Vector2f((int) Math.round((change.x - 16f) / 16f) * 16f + 16f, (int) Math.round((change.y - 16f) / 16f) * 16f + 16f);
			position(position().add(change));
		}
	}
	
	@Override
	public Entity opacity(float a) {
		return super.opacity(Math.min(a, 0.8f));
	}
	
	public String toString() {
		return "[InvisibleLogic #" + fID + "]";
	}
}

package com.pointcliki.dizgruntled.rez;

import java.io.ByteArrayInputStream;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class MonolithWAV {

	protected MonolithFile fFile;
	protected Sound fSound;
	
	public MonolithWAV(MonolithFile file) {
		fFile = file;
	}
	
	public Sound sound() {
		if (fSound != null) return fSound;
		if (fFile == null) return null;
		try {
			// TODO: Pick a sensible sound name
			fSound = new Sound(new ByteArrayInputStream(fFile.data()), "sample.wav");
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return fSound;
	}
}


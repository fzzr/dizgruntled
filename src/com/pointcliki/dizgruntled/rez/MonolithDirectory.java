package com.pointcliki.dizgruntled.rez;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Set;

public class MonolithDirectory {

	protected RandomAccessFile fFile;
	protected HashMap<String, MonolithDirectory> fChildDirs = null;
	protected HashMap<String, MonolithFile> fChildFiles = null;
	protected int fOffset;
	protected int fSize;
	
	public MonolithDirectory(RandomAccessFile file, int offset, int size) {
		fFile = file;
		fOffset = offset;
		fSize = size;
	}
	
	protected int readInteger() throws IOException {
		return fFile.read() + (fFile.read() << 8) + (fFile.read() << 16) + (fFile.read() << 24);
	}
	
	protected int readNode() throws IOException {
		boolean dir = readInteger() == 1;
		int read = 4;
		if (dir) {
			int dirOffset = readInteger();
			read += 4;
			int dirLength = readInteger();
			read += 4;
			// Ditch date / time
			readInteger();
			read += 4;
			// Read out filename
			StringBuilder sb = new StringBuilder();
			while (true) {
				read++;
				int val = fFile.read();
				if (val == 0) break;
				sb.append((char) val);
			}
			String filename = sb.toString();
			// Print
			fChildDirs.put(filename, new MonolithDirectory(fFile, dirOffset, dirLength));
		} else {
			int fileOffset = readInteger();
			read += 4;
			int fileLength = readInteger();
			read += 4;
			// Ditch date / time
			readInteger();
			read += 4;
			// Ditch id
			readInteger();
			read += 4;
			// Extension
			String fileExt = "." + ((char) fFile.read()) + ((char) fFile.read()) + ((char) fFile.read()) + ((char) fFile.read());
			read += 4;
			// Ditch null
			readInteger();
			read += 4;
			// Read out filename
			StringBuilder sb = new StringBuilder();
			while (true) {
				read++;
				int val = fFile.read();
				if (val == 0) break;
				sb.append((char) val);
			}
			String filename = sb.toString();
			// Ditch trailing null byte
			fFile.read();
			read++;
			// Print
			fChildFiles.put(filename, new MonolithFile(fFile, fileExt, fileOffset, fileLength));
		}
		return read;
	}
	
	public Set<String> directories() {
		try {
			if (fChildDirs == null) init();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return fChildDirs.keySet();
	}
	
	public Set<String> files() {
		try {
			if (fChildDirs == null) init();
		} catch (IOException e) {
			return null;
		}
		return fChildFiles.keySet();
	}
	
	public MonolithDirectory directory(String name) {
		try {
			if (fChildDirs == null) init();
		} catch (IOException e) {
			return null;
		}
		return fChildDirs.get(name);
	}
	
	public MonolithFile file(String name, String dir) {
		try {
			if (fChildDirs == null) init();
		} catch (IOException e) {
			return null;
		}
		return fChildFiles.get(name);
	}
	
	protected MonolithFile resolve(String[] path, int off) {
		if (off == path.length - 1) return file(path[off], null);
		MonolithDirectory d = directory(path[off]);
		if (d != null) return d.resolve(path, off + 1);
		return null;
	}
	
	protected MonolithDirectory resolveDirectory(String[] path, int off) {
		if (off == path.length - 1) return directory(path[off]);
		MonolithDirectory d = directory(path[off]);
		if (d != null) return d.resolveDirectory(path, off + 1);
		return null;
	}

	protected void init() throws IOException {
		fChildDirs = new HashMap<String, MonolithDirectory>();
		fChildFiles = new HashMap<String, MonolithFile>();
		
		// Skip to nodes
		fFile.seek(fOffset);
		
		int i = fSize;
		while (i > 0) {
			i -= readNode();
		}
	}

}

package com.pointcliki.dizgruntled.editor;

import java.io.File;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.grid.GridManager;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.Hyperlink;
import com.pointcliki.ui.Hyperlink.HyperlinkEvent;
import com.pointcliki.ui.ScrollBar;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.core.EntityQuery;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.core.Scene;
import com.pointcliki.core.Sprite;
import com.pointcliki.core.TextEntity;
import com.pointcliki.core.TimeManager.TimeEvent;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.map.MapEntity;
import com.pointcliki.dizgruntled.map.GruntzMapManager;
import com.pointcliki.ui.SelectionEvent;

public class EditorScene extends Scene {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1L;
	
	protected static UnicodeFont sFont;
	protected static UnicodeFont sFont2;
	
	protected Sprite fTopBar;
	private TextEntity fTitle;
	private Sprite fSideBar;
	private UIEntity fSideContainer = null;
	private MapEntity fMapViewer;
	private Sprite fCursor;
	private AnimatedSprite fAniCursor;
	private ScrollBar fScrollY;
	private ScrollBar fScrollX;
	private EntityQuery<Hyperlink> fLinks;
	private Logic fSelectedLogic;
	private int fSW;
	private int fSH;

	private GruntzMapManager fMapManager;
	private GridManager fGridManager;
	
	private UIEntity fSave;

	public EditorScene(PointClikiGame game) {
		super(game);
		registerUIMinion();
	}
	
	public void init() {
		super.init();
		
		initFonts();
				
		fSW = fGame.application().getWidth();
		fSH = fGame.application().getHeight();
		
		fTopBar = new Sprite("editor/topbar");
		fTopBar.resize(new Vector2f(fSW, 28));
		
		fTitle = new TextEntity("Dizgruntled Level Editor", TextEntity.ALIGN_LEFT, sFont, new Color(255, 220, 50));
		fTitle.position(new Vector2f(5, 13));
		
		fSave = new SaveButton();
		fSave.position(new Vector2f(192, 8));
		
		fLinks = new EntityQuery<Hyperlink>();
		addChild(fLinks.add(new Hyperlink("Level", "level", sFont, true)).position(new Vector2f(210, 4)), 104);
		addChild(fLinks.add(new Hyperlink("Tiles", "tiles", sFont)).position(new Vector2f(255, 4)), 105);
		addChild(fLinks.add(new Hyperlink("Smarts", "smarts", sFont)).position(new Vector2f(297, 4)), 106);
		addChild(fLinks.add(new Hyperlink("Logics", "logics", sFont)).position(new Vector2f(358, 4)), 107);
		
		Minion<IEvent> clicker = new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (!(event instanceof HyperlinkEvent)) return Minion.CONTINUE;
				HyperlinkEvent ev = (HyperlinkEvent) event; 
				for (Hyperlink h: fLinks) if (!h.equals(ev.hyperlink())) h.deselect();
				
				boolean showLogics = false;
				boolean showCursor = false;
				
				if (fAniCursor != null) {
					fAniCursor.cleanup();
					fAniCursor = null;
				}
				
				// Create correct side bar
				if (ev.href().equals("level")) {
					LevelSideBar s = new LevelSideBar();
					setSideContainer(s);
					s.init();
					showLogics = true;
				} else if (ev.href().equals("tiles")) {
					TilesSideBar s = new TilesSideBar();
					setSideContainer(s);
					s.init();
					showCursor = true;
				} else if (ev.href().equals("smarts")) {
					SmartsSideBar s = new SmartsSideBar();
					setSideContainer(s);
					s.init();
					showCursor = true;
				} else if (ev.href().equals("logics")) {
					ObjectSideBar s = new ObjectSideBar();
					setSideContainer(s);
					s.init();
					if (fSelectedLogic != null) s.editLogic(fSelectedLogic);
					showLogics = true;
				}
				if (showLogics) fMapViewer.showLogics();
				else fMapViewer.hideLogics();
				if (showCursor) addChild(fCursor, 20);
				else removeChild(fCursor);
								
				return Minion.CONTINUE;
			}
		};
		for (Hyperlink h: fLinks) h.dispatcher().addMinion("hyperlink", clicker);
		
		fSideBar = new Sprite("editor/sidebar");
		fSideBar.position(new Vector2f(fSW - 164, 28));
		fSideBar.resize(new Vector2f(164, fSH - 28));
		
		addChild(fTopBar, 100);
		addChild(fTitle, 101);
		addChild(fSave, 102);
		addChild(fSideBar, 103);
		
		fMapManager.importLevel(GruntzGame.resourceManager().rez().file("AREA1/WORLDZ/LEVEL1", "wwd"));
		
		// Scroll bars
		fScrollX = new ScrollBar(new Minion<SelectionEvent>(){
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				fMapViewer.offset(new Vector2f(16 - event.value(), fMapViewer.offset().y));
				return Minion.CONTINUE;
			}
		}, false);
		fScrollX.resize(new Vector2f(fSW - 176, 12));
		fScrollX.position(new Vector2f(0, fSH - 12));
		
		fScrollX.barSpan(fSW - 176);
		addChild(fScrollX, 104);
		fScrollY = new ScrollBar(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				fMapViewer.offset(new Vector2f(fMapViewer.offset().x, 16 - event.value()));
				return Minion.CONTINUE;
			}
		}, true);
		fScrollY.resize(new Vector2f(12, fSH - 40));
		fScrollY.position(new Vector2f(fSW - 176, 28));
		fScrollY.barSpan(fSH - 44);
		addChild(fScrollY, 105);
		
		try {
			fCursor = new Sprite(new Image(0, 0)) {
				/**
				 * Serial Key
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void render(Graphics graphics, long currentTime) {				
					if (fImage.getWidth() != 0) super.render(graphics, currentTime);
					else {
						graphics.setColor(new Color(100, 100, 100, 200));
						graphics.fillRect(0, 0, 32, 32);
					}
				}
			};
			fCursor.opacity(0.8f);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		Minion<TimeEvent> scroller = new Minion<TimeEvent>() {
			public long run(Dispatcher<TimeEvent> dispatcher, String type, TimeEvent event) {
				
				if (fSideContainer.focused()) return Minion.CONTINUE;
				
				if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_RIGHT)) {
					fMapViewer.offset(new Vector2f(Math.min(Math.max(fMapViewer.offset().x - event.delta() / 2f, -fMapViewer.map().width() * 32f + fSW - 192), 16f), fMapViewer.offset().y));
					fScrollX.value((int) (16 - fMapViewer.offset().x));
				}
				else if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_LEFT)) {
					fMapViewer.offset(new Vector2f(Math.min(Math.max(fMapViewer.offset().x + event.delta() / 2f, -fMapViewer.map().width() * 32 + fSW - 192), 16f), fMapViewer.offset().y));
					fScrollX.value((int) (16 - fMapViewer.offset().x));
				}
				if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_DOWN)) {
					fMapViewer.offset(new Vector2f(fMapViewer.offset().x, Math.min(Math.max(fMapViewer.offset().y - event.delta() / 2f, -fMapViewer.map().height() * 32 + fSH - 60f), 16f)));
					fScrollY.value((int) (16 - fMapViewer.offset().y));
				}
				else if (GruntzGame.inputManager().isKeyPressed(Keyboard.KEY_UP)) {
					fMapViewer.offset(new Vector2f(fMapViewer.offset().x, Math.min(Math.max(fMapViewer.offset().y + event.delta() / 2f, -fMapViewer.map().height() * 32 + fSH - 60f), 16f)));
					fScrollY.value((int) (16 - fMapViewer.offset().y));
				}
				return Minion.CONTINUE;				
			};
		};
		timeManager().dispatcher().addMinion(TimeEvent.TYPE, scroller);
		
		LevelSideBar t = new LevelSideBar();
		setSideContainer(t);
		t.init();
		
		setupMapViewer();
		
		//addChild(new Sprite("pointcliki").position(new Vector2f(300, 300)), 200);
		
		fTimeManager.start();
	}
	
	private void setupMapViewer() {
		fMapManager.map().editing(true);

		fMapViewer = new MapEntity(fMapManager.map(), new Vector2f(fSW - 176, fSH - 40));
		fMapViewer.offset(new Vector2f(16, 16));
		fMapViewer.position(new Vector2f(0, 28));
		addChild(fMapViewer, 0);
		fMapViewer.init();
		
		PointClikiGame.instance().application().setTitle(fMapViewer.map().name() + " - Dizgruntled Level Editor");
		
		fMapViewer.dispatcher().addMinion("mouse.move", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof MouseEvent) {
					Vector2f position = fMapViewer.positionOfTile(fMapViewer.tileAtPoint(new Vector2f(((MouseEvent)event).x(), ((MouseEvent)event).y() - 28)));
					fCursor.position(position.add(new Vector2f(0, 28)));
					if (fAniCursor != null) fAniCursor.position(new Vector2f(((MouseEvent) event).x(), ((MouseEvent) event).y()));
				}
				return Minion.CONTINUE;
			}
		});
		
		fMapViewer.dispatcher().addMinion("mouse.down", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof MouseEvent && fSideContainer instanceof TilesSideBar) {
					GridCoordinate g = fMapViewer.tileAtPoint(new Vector2f(((MouseEvent)event).x(), ((MouseEvent)event).y() - 28));
					TilesSideBar side = (TilesSideBar) fSideContainer;
					fMapViewer.map().tile(side.tileset(), g.x(), g.y(), side.selected());
				}
				if (fAniCursor != null) {
					if (((MouseEvent) event).button() == 1) setEditingLogic(null);
					else ((ObjectSideBar) fSideContainer).placeLogic(new Vector2f(((MouseEvent) event).x() - fMapViewer.offset().x, ((MouseEvent) event).y() - fMapViewer.offset().y - 28));
				}
				return Minion.CONTINUE;
			}
		});
		
		fMapViewer.dispatcher().addMinion("mouse.drag", new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof MouseEvent && fSideContainer instanceof TilesSideBar) {
					GridCoordinate g = fMapViewer.tileAtPoint(new Vector2f(((MouseEvent)event).x(), ((MouseEvent)event).y() - 28));
					TilesSideBar side = (TilesSideBar) fSideContainer;
					fMapViewer.map().tile(side.tileset(), g.x(), g.y(), side.selected());
					fCursor.position(fMapViewer.positionOfTile(g).add(new Vector2f(0, 28)));
				}
				return Minion.CONTINUE;
			}
		});
		
		fScrollX.ticks(fMapViewer.map().width() * 32 + 32);
		fScrollX.value(0);
		fScrollY.ticks(fMapViewer.map().height() * 32 + 32);
		fScrollY.value(0);
	}
	
	public void newLevel(String world, int x, int y) {
		fMapViewer.cleanup();
		fMapManager.newLevel(world, x, y);
		setupMapViewer();
	}
	public void loadLevel(File f) {
		fMapViewer.cleanup();
		fMapManager.load(f);
		setupMapViewer();
	}
	public void importLevel(String s) {
		fMapViewer.cleanup();
		fMapManager.importLevel(GruntzGame.resourceManager().rez().file(s, "wwd"));
		setupMapViewer();
	}
	public void playLevel() {
		fMapManager.save();
		if (fMapManager.file() != null) GruntzGame.instance().playLevel(fMapManager.file());
	}
	
	public MapEntity mapViewer() {
		return fMapViewer;
	}
	
	public Sprite cursor() {
		return fCursor;
	}
	
	public void setSideContainer(UIEntity c) {
		if (fSideContainer != null) removeChild(fSideContainer);
		
		fSideContainer = c;
		c.position(new Vector2f(PointClikiGame.instance().application().getWidth() - 159, 32));
		addChild(fSideContainer, 130);
	}
	
	public void setEditingLogic(Logic l) {
		ObjectSideBar o = null;
		fSelectedLogic = l;
		if (fSideContainer instanceof ObjectSideBar) o = (ObjectSideBar) fSideContainer;
		if (o != null) o.editLogic(l);
	}
	
	public void setAniCursor(AnimatedSprite s) {
		if (fAniCursor != null) removeChild(fAniCursor);
		fAniCursor = s;
		if (s != null) {
			fMapViewer.disableLogicEditing();
			addChild(s, 20);
		} else {
			fMapViewer.enableLogicEditing();
		}
	}

	@SuppressWarnings("unchecked")
	private void initFonts() {
		try {
			sFont = new UnicodeFont("lib/sundaycomicsbb_reg.ttf", 14, false, false);
			sFont.getEffects().add(new ColorEffect(java.awt.Color.white));
			sFont.addAsciiGlyphs();
			sFont.loadGlyphs();
			
			sFont2 = new UnicodeFont("lib/sundaycomicsbb_reg.ttf", 24, false, false);
			sFont2.getEffects().add(new ColorEffect(java.awt.Color.white));
			sFont2.addAsciiGlyphs();
			sFont2.loadGlyphs();
		} catch (Exception e) {
			System.err.println("Failed to load font");
		}
	}
	
	public static UnicodeFont LargeFont() {
		return sFont2;
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		graphics.setColor(new Color(200, 200, 200));
		graphics.fillRect(fSW - 176, fSH - 12, 12, 12);
	}
	
	@Override
	public void configureManagers() {
		super.configureManagers();
		fMapManager = new GruntzMapManager(this);
		fGridManager = new GridManager(this, new Vector2f(32f, 32f), new Vector2f(16f, 16f));
		fManagers.put(GruntzMapManager.class, fMapManager);
		fManagers.put(GridManager.class, fGridManager);
	}
	
	public GridManager gridManager() {
		return fGridManager;
	}
	
	public GruntzMapManager mapManager() {
		return fMapManager;
	}
	
	public Map map() {
		return fMapManager.map();
	}
	
	@Override
	public String toString() {
		return "[Editor Scene " + fID + "]";
	}
}

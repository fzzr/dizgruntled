package com.pointcliki.dizgruntled.editor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.ui.Button;
import com.pointcliki.ui.DropDownMenu;
import com.pointcliki.ui.TextBox;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.core.Entity;
import com.pointcliki.core.Sprite;
import com.pointcliki.core.TextEntity;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.LogicFactory;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.ui.SelectionEvent;

public class ObjectSideBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 507402225763963263L;
	
	protected String fType = "Scenery";
	protected String fSubType = "Rocky Roadz";
	protected String fItem = "";
	protected JSONObject fLogics;
	protected LogicPane fLogicPane;
	
	public void init() {
		resize(new Vector2f(164, 740));
		// Load scenery
		try {
			fLogics = new JSONObject(new JSONTokener(new FileReader("config/logics.json")));
			
		} catch (FileNotFoundException e) {
			System.err.println("Can't find logics.json");
			System.err.println(e.getMessage());
		} catch (JSONException e) {
			System.err.println("Can't parse logics.json");
			System.err.println(e.getMessage());
		}
				
		// Create the logics
		createLogicEditor();
	}
	
	private void createLogicEditor() { 
		fItem = "";
		// Add settings
		final DropDownMenu type = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String t, SelectionEvent event) {
				fType = ((DropDownMenu) event.source()).value();
				try {
					fSubType = JSONObject.getNames(fLogics.getJSONObject(fType))[0];
				} catch (JSONException e) {
					System.err.println("Can't parse subtype");
					System.err.println(e.getMessage());
				}
				editLogic(null);
				return Minion.CONTINUE;
			}
		});
		for (String s: JSONObject.getNames(fLogics)) type.add(s);
		type.select(fType);
		type.resize(new Vector2f(155, 24));
		addChild(type, 10);
		
		final DropDownMenu subtype = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				fSubType = ((DropDownMenu) event.source()).value();
				editLogic(null);
				return Minion.CONTINUE;
			}
		});
		subtype.position(new Vector2f(0, 26));
		try {
			for (String s: JSONObject.getNames(fLogics.getJSONObject(fType))) subtype.add(s);
		} catch (JSONException e) {
			System.err.println("Error parsing the editor preset logics");
			System.err.println(e.getMessage());
		}
		subtype.select(fSubType);
		subtype.resize(new Vector2f(155, 24));
		addChild(subtype, 9);
		
		fLogicPane = new LogicPane();
		fLogicPane.position(new Vector2f(0, 56));
		addChild(fLogicPane);		
		fLogicPane.refresh();
	}
	
	public void editLogic(final Logic l) {
		for (Entity e: this) removeChild(e);
		scene(EditorScene.class).setAniCursor(null);
		
		if (l == null) {
			createLogicEditor();
			return;
		}
		
		// Add settings
		Button create = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				Map map = scene(EditorScene.class).map();
				for (Logic l: map.selectedLogics()) map.deselectLogic(l);
				return Minion.CONTINUE;
			}
		}, "new logic");
		create.resize(new Vector2f(155, 24));
		addChild(create, 2);
		
		Button delete = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				l.cleanup();
				return Minion.CONTINUE;
			}
		}, "delete logic");
		delete.resize(new Vector2f(155, 24));
		delete.position(new Vector2f(0, 26));
		addChild(delete, 3);
		
		TextEntity name = new TextEntity(l.name(), TextEntity.ALIGN_LEFT, EditorScene.LargeFont(), Color.darkGray);
		name.position(new Vector2f(0, 64));
		addChild(name, 4);
		
		int i = 80;
		int n = 100;
		// Add properties
		if (l.properties() == null) return;
		for (LogicProperty p: l.properties()) {
			if (p instanceof StringLogicProperty) {
				final StringLogicProperty s = (StringLogicProperty) p;
				// Add label
				TextEntity label = new TextEntity(s.name(), TextEntity.ALIGN_LEFT, GruntzGame.resourceManager().UIFont(), Color.darkGray);
				label.position(new Vector2f(2, i + 14));
				addChild(label, n);
				n--;
				i += 26;
				
				String[] choices = s.choices();
				if (choices == null) {
					TextBox box = new TextBox(s.value().equals("") ? s.name() : s.value(), s.name());
					box.position(new Vector2f(0, i)).resize(new Vector2f(155, 24));
					box.setChangedMinion(new Minion<IEvent>() {
						@Override
						public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
							s.value(type);
							editLogic(l);
							return Minion.CONTINUE;
						}
					});
					addChild(box, n);
				} else {
					DropDownMenu drop = new DropDownMenu(new Minion<SelectionEvent>() {
						@Override
						public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
							s.choice(event.value());
							editLogic(l);
							return Minion.CONTINUE;
						}
					});
					drop.position(new Vector2f(0, i)).resize(new Vector2f(155, 24));
					for (String str: choices) drop.add(str.toLowerCase());
					if (!s.value().equals("")) drop.select(s.value().toLowerCase());
					addChild(drop, n);
				}
			}
			n--;
			i += 26;
		}
	}
	
	protected class LogicPane extends UIEntity {
		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -8225255026907646983L;
		
		protected ArrayList<LogicSprite> fSprites = new ArrayList<LogicSprite>();

		public void refresh() {
			for (Entity e: this) e.cleanup();
			int x = 0;
			int y = 0;
			int maxh = 0;
			
			LogicSpriteFactory f = new LogicSpriteFactory();
			
			try {
				JSONObject group = fLogics.getJSONObject(fType).getJSONObject(fSubType);
				JSONObject logs = group.getJSONObject("logics");
				for (String name: JSONObject.getNames(logs)) {
					
					JSONObject j = new JSONObject();					
					JSONObject obj = logs.getJSONObject(name);
					// Copy information over from group
					for (String s: JSONObject.getNames(group)) if (!s.equals("logics")) j.put(s, group.get(s));
					// Copy information over from object
					for (String s: JSONObject.getNames(obj)) j.put(s, obj.get(s));
					
					LogicSprite sp = new LogicSprite(name, f.spriteFromJSON(j));
					if (sp != null) {
						if (x + sp.span().getWidth() + 5 > 138) {
							x = 0;
							y += maxh + 5;
							maxh = (int) sp.span().getHeight();
						} else {
							maxh = (int) Math.max(maxh, sp.span().getHeight());
						}
						sp.position(new Vector2f(x, y));
						x += sp.span().getWidth() + 5;
						addChild(sp);
						fSprites.add(sp);
					}
				}
				
			} catch (JSONException e) {
				System.err.println("Problem parsing subtype");
				System.err.println(e.getMessage());
			}
			fSpan = new Rectangle(0, 0, 138, y + 130);
		}
		
		protected class LogicSprite extends UIEntity {
			
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 3383348913157259573L;
			
			protected boolean fSelected = false;
			protected AnimatedSprite fSprite;
			protected String fName;

			public LogicSprite(String name, AnimatedSprite sp) {
				fSprite = sp;
				Sprite s = new Sprite(fSprite.image(0));
				fSpan = s.span();
				fName = name;
				addChild(s);
			}
			
			@Override
			public void focus() {
				super.focus();
				for (LogicSprite ls: LogicPane.this.fSprites) ls.blur();
				ObjectSideBar.this.scene(EditorScene.class).setAniCursor(fSprite);
				ObjectSideBar.this.fItem = fName;
				fSelected = true;
			}
			
			@Override
			public void blur() {
				super.blur();
				fSelected = false;
			}
			
			@Override
			public void render(Graphics graphics, long currentTime) {
				super.render(graphics, currentTime);
				if (fSelected) {
					graphics.setColor(Color.darkGray);
					graphics.drawRect(-1,  -1,  fSpan.getWidth() + 2, fSpan.getHeight() + 2);
					graphics.drawRect(-3,  -3,  fSpan.getWidth() + 6, fSpan.getHeight() + 6);
				}
			}
			
			public AnimatedSprite sprite() {
				return fSprite;
			}
		}
	}

	public void placeLogic(Vector2f v) {
		
		JSONObject j = new JSONObject();
		try {
			// Put position
			j.put("xy", v.x + " " + v.y);
			JSONObject group = fLogics.getJSONObject(fType).getJSONObject(fSubType);
			JSONObject obj = group.getJSONObject("logics").getJSONObject(fItem);
			// Copy information over from group
			for (String s: JSONObject.getNames(group)) if (!s.equals("logics")) j.put(s, group.get(s));
			// Copy information over from object
			for (String s: JSONObject.getNames(obj)) j.put(s, obj.get(s));
		} catch (JSONException e) {
			System.err.println("Problem placing logic");
			System.err.println(e.getMessage());
		}
		// Create a logic factory to handle the creation of in-game logics
		LogicFactory factory = new LogicFactory();
		scene(EditorScene.class).map().placeLogic(factory.createFromJSON(j));
	}
}
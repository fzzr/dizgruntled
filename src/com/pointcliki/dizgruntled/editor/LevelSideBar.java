package com.pointcliki.dizgruntled.editor;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.ui.Button;
import com.pointcliki.ui.DropDownMenu;
import com.pointcliki.ui.SelectionEvent;
import com.pointcliki.ui.TextBox;
import com.pointcliki.ui.UIEntity;
import com.pointcliki.dizgruntled.map.Map;

public class LevelSideBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 2986159454099694896L;
	
	public static final String[] IMPORTS = new String[] {
		"AREA1/TRAINING1",
		"AREA1/TRAINING2",
		"AREA1/TRAINING3",
		"AREA1/TRAINING4",
		"AREA1/LEVEL1",
		"AREA1/LEVEL2",
		"AREA1/LEVEL3",
		"AREA1/LEVEL4",
		"AREA1/LEVEL101",
		"AREA2/LEVEL5",
		"AREA2/LEVEL6",
		"AREA2/LEVEL7",
		"AREA2/LEVEL8",
		"AREA3/LEVEL9",
		"AREA3/LEVEL10",
		"AREA3/LEVEL11",
		"AREA3/LEVEL12",
		"AREA4/LEVEL13",
		"AREA4/LEVEL14",
		"AREA4/LEVEL15",
		"AREA4/LEVEL16",
		"AREA5/LEVEL17",
		"AREA5/LEVEL18",
		"AREA5/LEVEL19",
		"AREA5/LEVEL20",
		"AREA6/LEVEL21",
		"AREA6/LEVEL22",
		"AREA6/LEVEL23",
		"AREA6/LEVEL24",
		"AREA7/LEVEL25",
		"AREA7/LEVEL26",
		"AREA7/LEVEL27",
		"AREA7/LEVEL28",
		"AREA8/LEVEL29",
		"AREA8/LEVEL30",
		"AREA8/LEVEL31",
		"AREA8/LEVEL32"
	};

	public void init() {
		resize(new Vector2f(164, 740));
		
		final TextBox title = new TextBox(scene(EditorScene.class).map().name(), "level title");
		title.resize(new Vector2f(155, 24));
		addChild(title, 1);
		title.setChangedMinion(new Minion<IEvent> () {
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				scene(EditorScene.class).map().name(title.value());
				return Minion.CONTINUE;
			};
		});
		
		final TextBox author = new TextBox(scene(EditorScene.class).map().author(), "author");
		author.resize(new Vector2f(155, 24));
		author.position(new Vector2f(0, 26));
		addChild(author, 1);
		author.setChangedMinion(new Minion<IEvent> () {
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				scene(EditorScene.class).map().author(author.value());
				return Minion.CONTINUE;
			};
		});
		
		TextBox dimensions = new TextBox(scene(EditorScene.class).map().width() + " " + scene(EditorScene.class).map().height(), "dimensions");
		dimensions.resize(new Vector2f(155, 24));
		dimensions.position(new Vector2f(0, 52));
		addChild(dimensions, 1);
		
		TextBox test4 = new TextBox("camera location");
		test4.resize(new Vector2f(155, 24));
		test4.position(new Vector2f(0, 78));
		addChild(test4, 1);
		
		final TextBox quirks = new TextBox(scene(EditorScene.class).map().quirks(), "quirkz mode");
		quirks.resize(new Vector2f(155, 24));
		quirks.position(new Vector2f(0, 104));
		addChild(quirks, 1);
		quirks.setChangedMinion(new Minion<IEvent> () {
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				scene(EditorScene.class).map().quirks(quirks.value());
				return Minion.CONTINUE;
			};
		});
		
		final DropDownMenu type = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String t, SelectionEvent event) {
				scene(EditorScene.class).map().type(((DropDownMenu)event.source()).value());
				return Minion.CONTINUE;
			}
		});
		type.add("Questz");
		type.add("Friendz");
		type.add("Battlez");
		type.select(scene(EditorScene.class).map().type());
		type.resize(new Vector2f(155, 24));
		type.position(new Vector2f(0, 130));
		addChild(type, 3);
		
		DropDownMenu fog = new DropDownMenu(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String t, SelectionEvent event) {
				scene(EditorScene.class).map().fog(((DropDownMenu)event.source()).value());
				return Minion.CONTINUE;
			}
		});
		fog.add("No Fog");
		fog.add("Fog Of War");
		fog.add("Fixed Fog");
		fog.select(scene(EditorScene.class).map().fog());
		fog.resize(new Vector2f(155, 24));
		fog.position(new Vector2f(0, 156));
		addChild(fog, 2);
		
		Button play = new Button(new Minion<SelectionEvent>() {
			
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				scene(EditorScene.class).playLevel();
				return Minion.CONTINUE;
			}
			
		}, "play");
		play.resize(new Vector2f(155, 24));
		play.position(new Vector2f(0, 182));
		addChild(play, 1);
		
		Button create = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				// Get the world
				JFrame frame = new JFrame();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
				frame.setAlwaysOnTop(true);
				String w = (String) JOptionPane.showInputDialog(frame, "Choose the world of the level", "New Level", JOptionPane.INFORMATION_MESSAGE, null, Map.WORLDS, "Rocky Roadz");
				frame.dispose();
				if (w == null) return Minion.CONTINUE;
				
				// Get the dimensions
				frame = new JFrame();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
				frame.setAlwaysOnTop(true);
				String s = (String) JOptionPane.showInputDialog(frame, "Enter the dimensions of the level in tiles: x y", "New Level", JOptionPane.INFORMATION_MESSAGE);
				frame.dispose();
				if (s == null) return Minion.CONTINUE;
				String[] d = s.split(" ");
				try {
					int x = Integer.parseInt(d[0]);
					int y = Integer.parseInt(d[1]);
					
					scene(EditorScene.class).newLevel(w, x, y);
					
				} catch (Exception e) {
					System.err.println("Bad dimensions!");
				}
				return Minion.CONTINUE;
			}
		}, "new level");
		create.resize(new Vector2f(155, 24));
		create.position(new Vector2f(0, 208));
		addChild(create, 1);
		
		Button open = new Button(new Minion<SelectionEvent>() {
		
			@Override
			public long run(Dispatcher<	SelectionEvent> dispatcher, String type, 	SelectionEvent event) {
				
				JFileChooser loader = new JFileChooser();
				loader.setAcceptAllFileFilterUsed(false);
				loader.addChoosableFileFilter(new FileNameExtensionFilter("Dizgruntled Maps (*.map)", "map"));
				loader.addChoosableFileFilter(new FileNameExtensionFilter("Wap World Maps (*.wwd)", "wwd"));
				loader.addChoosableFileFilter(new FileNameExtensionFilter("All Maps", "wwd", "map"));
				loader.setDialogTitle("Open Dizgruntled Map");
				JFrame frame = new JFrame();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
				frame.setAlwaysOnTop(true);
				int r = loader.showOpenDialog(frame);
				frame.dispose();
				if (r == JFileChooser.APPROVE_OPTION) scene(EditorScene.class).loadLevel(loader.getSelectedFile());
				return Minion.CONTINUE;
			}
			
		}, "open level");
		open.resize(new Vector2f(155, 24));
		open.position(new Vector2f(0, 234));
		addChild(open, 1);
		
		Button save = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				scene(EditorScene.class).mapManager().saveAs();
				return Minion.CONTINUE;
			}			
		}, "save level as");
		save.resize(new Vector2f(155, 24));
		save.position(new Vector2f(0, 260));
		addChild(save, 1);
		
		Button imp = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				JFrame frame = new JFrame();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
				frame.setAlwaysOnTop(true);
				String s = (String) JOptionPane.showInputDialog(frame, "Choose a level from the original game to import", "Import Level", JOptionPane.INFORMATION_MESSAGE, null, IMPORTS, "AREA1/TRAINING1");
				frame.dispose();
				if (s == null) return Minion.CONTINUE;
				String[] opt = s.split("/");
				s = opt[0] + "/WORLDZ/" + opt[1];
				scene(EditorScene.class).importLevel(s);
				return Minion.CONTINUE;
			}			
		}, "import level");
		imp.resize(new Vector2f(155, 24));
		imp.position(new Vector2f(0, 286));
		addChild(imp, 1);

		Button exit = new Button(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				System.exit(0);
				return Minion.CONTINUE;
			}			
		}, "exit");
		exit.resize(new Vector2f(155, 24));
		exit.position(new Vector2f(0, 312));
		addChild(exit, 1);
	}
}
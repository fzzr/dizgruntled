package com.pointcliki.dizgruntled.modifier;


public class AssignModifier<T> extends Modifier<T> implements Cloneable {

	private T fAssign;
	
	public AssignModifier(long p, T val) {
		super(p);
		fAssign = val;
	}

	@Override
	public T modify(T obj) {
		return fAssign;
	}
	
	public void assign(T val) {
		fAssign = val;
	}

	public T value() {
		return fAssign;
	}
}



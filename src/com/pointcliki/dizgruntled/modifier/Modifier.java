package com.pointcliki.dizgruntled.modifier;

public abstract class Modifier<T> implements Cloneable, Comparable<Modifier<? extends Object>> {
	
	protected long priority = 0;
	
	public Modifier (long p) {
		priority = p;
	}
	
	public int compareTo(Modifier<? extends Object> m) {
		if (priority > m.priority) return 1;
		if (priority < m.priority) return -1;
		return 0;
	}

	public abstract T modify(T val);

	public long priority() {
		return priority;
	}
}

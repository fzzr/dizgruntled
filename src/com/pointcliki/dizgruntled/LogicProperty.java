package com.pointcliki.dizgruntled;

public abstract class LogicProperty {
	
	protected String fName;
	
	public LogicProperty(String name) {
		fName = name;
	}

	/**
	 * @return The name of the property
	 */
	public String name() {
		return fName;
	}
		
	/**
	 * @return A description of the property
	 */
	public abstract String description();
	
	/**
	 * @return The current value of the property 
	 */
	public abstract Object value();
}

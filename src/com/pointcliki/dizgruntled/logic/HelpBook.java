package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GridLogic;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;

/**
 * A logic to display a help book
 * 
 * @author Hugheth
 * @since alpha 2.6
 */
public class HelpBook extends GridLogic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	protected AnimatedSprite fAnimation;
	protected String fText;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		super.importFromWWD(logic, image, animation, data);
		int ref = MonolithWWD.readSmarts(data);
		fText = GruntzGame.resourceManager().textString(ref);
		if (fText == null) fText = "No text";
		updateAnimation();
	}

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		super.importFromJSON(object);
		fText = object.optString("text", fText);
		updateAnimation();
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		o.put("text", fText);
		return o;
	}

	private void updateAnimation() {
		
		fAnimation = MonolithANI.fromDirectory("GAME/IMAGEZ/HELPBOX");
		
		addChild(fAnimation);
		fSpan = fAnimation.span();
		if (fMap != null) {
			fAnimation.start();
		}
	}
	
	@Override
	public void init(Map map) {
		super.init(map);
		fAnimation.start();
	}

	@Override
	public void cleanup() {
		fAnimation.cleanup();
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[HelpBook]";
	}

	@Override
	public void initProperties() {
		StringLogicProperty text = new StringLogicProperty("text") {
			
			@Override
			public String description() {
				return "The text in the book";
			}
			
			@Override
			public String value() {
				return fText;
			}
			
			@Override
			public String[] choices() {
				return null;
			}

			@Override
			public void value(String s) {
				fText = s;
			}
		};
		fProperties = new LogicProperty[]{text};
	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/HELPBOX/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "HelpBook";
	}

	public String text() {
		return fText;
	}
}

package com.pointcliki.dizgruntled.logic;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.player.GruntzPlayer;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;

/**
 * A logic to display the level fortress flags
 * 
 * @author Hugheth
 * @since alpha 2.6
 */
public class FortressFlag extends Logic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	protected AnimatedSprite fAnimation;
	protected String fSource;
	protected String fTeam;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		super.importFromWWD(logic, image, animation, data);
		fSource = "internal";
		fTeam = GruntzPlayer.TEAMS[MonolithWWD.readSmarts(data)];
		
		updateAnimation();
	}
		
	public void init(Map map) {
		super.init(map);
		updateAnimation();
		fAnimation.start();
	}

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		super.importFromJSON(object);
		fSource = object.optString("source", "internal");
		fTeam = object.optString("team", "KING");
		updateAnimation();
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		if (!fTeam.equals("KING")) o.put("team", fTeam);
		if (!fSource.equals("internal")) o.put("source", fSource);
		return o;
	}

	private void updateAnimation() {
		AnimatedSprite sprite = MonolithANI.fromDirectory("GAME/IMAGEZ/FORTRESSFLAGZ/" + fTeam);
		
		if (fAnimation != null) fAnimation.cleanup();
		fAnimation = sprite;
		addChild(fAnimation);
		fSpan = fAnimation.span();
		if (fMap != null) fAnimation.start();
	}

	@Override
	public void cleanup() {
		fAnimation.cleanup();
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[Fortress Flag]";
	}

	@Override
	public void initProperties() {
		StringLogicProperty team = new StringLogicProperty("team") {
			
			@Override
			public String description() {
				return "The team which the flag comes from";
			}
			
			@Override
			public String value() {
				return fTeam;
			}
			
			@Override
			public String[] choices() {
				return GruntzPlayer.TEAMS;
			}

			@Override
			public void choice(int i) {
				fTeam = GruntzPlayer.TEAMS[i];
				updateAnimation();
			}
		};
		fProperties = new LogicProperty[]{team};
	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/FORTRESSFLAGZ/" + object.optString("team", "KING") + "/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "FortressFlag";
	}
}

package com.pointcliki.dizgruntled.logic;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.Logic;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithFile;
import com.pointcliki.dizgruntled.rez.MonolithPID;

/**
 * A logic to display level scenery, such as EyeCandies and BehindAniCandies 
 * 
 * @author Hugheth
 * @since alpha 2.5
 */
public class Scenery extends Logic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	protected AnimatedSprite fAni;
	protected String fSource;
	protected String fRez;
	protected String fRezAni;
	protected String fArea;
	protected boolean fBehind = false;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {		
		if (!logic.equals("EyeCandy")) {
			fFixed = -1000;
			fBehind = true;
		}
		fSource = "internal";
		String[] s = image.split("/");
		fArea = s[0].substring(0, 5);
		fRez = s[2];
		if (!animation.equals("")) {
			s = animation.split("/");
			fRezAni = s[2];
		}
		
		super.importFromWWD(logic, image, animation, data);
		loadInternalScenery(image, animation);
	}
		
	public void init(Map map) {
		super.init(map);
		fAni.start();
	}

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		if (object.optBoolean("behind", false)) {
			fBehind = true;
			fFixed = -1000;
		}
		super.importFromJSON(object);
		
		fSource = object.optString("source", "internal");
		fArea = object.optString("area");
		fRez = object.optString("rez");
		fRezAni = object.optString("ani");
		
		if (fSource.equals("internal")) {
			String image = fArea + "/IMAGEZ/" + fRez;
			String animation = !object.optString("ani").equals("") ? fArea + "/ANIZ/" + fRezAni : "";
			loadInternalScenery(image, animation);
		}
	}
	
	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		o.put("behind", fBehind);
		if (!fSource.equals("internal")) o.put("source", fSource);
		if (fSource.equals("internal")) {
			o.putOpt("rez", fRez);
			o.putOpt("ani", fRezAni);
			o.putOpt("area", fArea);
		}
		return o;
	}
	
	protected void loadInternalScenery(String image, String animation) {
		if (animation.equals("")) {
			int j = 1;
			ArrayList<Image> images = new ArrayList<Image>();
			ArrayList<Vector2f> offsets = new ArrayList<Vector2f>();
			while (true) {
				String end = j + "";
				while (end.length() < 3) end = "0" + end;
				MonolithFile f = GruntzGame.resourceManager().rez().file(image + "/FRAME" + end, "pid");
				if (f == null) break;
				MonolithPID pid = new MonolithPID(f);
				images.add(pid.image());
				offsets.add(pid.offset());
				j++;
			}
			fAni = new AnimatedSprite(images.toArray(new Image[images.size()]), offsets.toArray(new Vector2f[offsets.size()]));
		} else {
			fAni = new MonolithANI(GruntzGame.resourceManager().rez().file(animation, "ani"), image).sprite();
		}
		fAni.frame((int) Math.floor(Math.random() * fAni.frames()));
		fSpan = fAni.span();
		addChild(fAni);
	}

	@Override
	public void cleanup() {
		fAni.cleanup();
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[Scenery]";
	}

	@Override
	public void initProperties() {}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		MonolithPID pid = GruntzGame.resourceManager().pid(object.getString("area") + "/IMAGEZ/" + object.getString("rez") + "/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "Scenery";
	}
}

package com.pointcliki.dizgruntled.logic;

import java.util.Arrays;

import javax.swing.JOptionPane;

import org.json.JSONException;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.AnimatedSprite;
import com.pointcliki.dizgruntled.GridLogic;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.LogicProperty;
import com.pointcliki.dizgruntled.StringLogicProperty;
import com.pointcliki.dizgruntled.map.Map;
import com.pointcliki.dizgruntled.player.HumanPlayer;
import com.pointcliki.dizgruntled.rez.MonolithANI;
import com.pointcliki.dizgruntled.rez.MonolithPID;
import com.pointcliki.dizgruntled.rez.MonolithWWD;

/**
 * A logic for InGame Icons such as tools, toys, powerups and utilities
 * 
 * @author Hugheth
 * @since alpha 2.5
 */
public class Pickup extends GridLogic {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5379476379535274956L;
	
	public static final String NONE = "NONE";
	
	public static final String[] TYPES = new String[] {"TOOL", "TOY", "POWERUP", "CURSE", "REWARD", "UTILITY"};
	public static final String[] TOOLS = new String[] {"BOMB", "BOOMERANG", "BRICK", "CLUB", "GAUNTLETZ", "GLOVEZ", "GOOBER", "GRAVITYBOOTZ", "GUNHAT", "NERFGUN", "ROCK", "SHIELD", "SHOVEL", "SPRING", "SPY", "SWORD", "TIMEBOMB", "TOOB", "WAND", "WARPSTONEZ1", "WARPSTONEZ2", "WARPSTONEZ3", "WARPSTONEZ4", "WELDER", "WINGZ"};
	public static final String[] TOYS = new String[] {"BABYWALKER", "BEACHBALL", "BIGWHEEL", "GOKART", "JACKINTHEBOX", "JUMPROPE", "POGOSTICK", "SCROLL", "SQUEAKTOY", "YOYO"};
	public static final String[] POWERUPS = new String[] {"CONVERSION", "DEATHTOUCH", "GHOST", "INVULNERABILITY", "REACTIVEARMOR", "ROIDZ", "SUPERSPEED"};
	public static final String[] CURSES = new String[] {"BLACKSCREEN", "MINICAM", "RANDOMCOLORZ", "SCREENSHAKE"};
	public static final String[] REWARDS = new String[] {"COIN", "SECRETW", "SECRETA", "SECRETR", "SECRETP"};
	public static final String[] UTILITIES = new String[] {"MEGAPHONEZ", "HEALTH1", "HEALTH2", "HEALTH3", "STOPWATCH", "TOYBOX", "TIMEBOMB"};
	
	public static final String[] MEGAPHONE_TYPES = new String[] {"TOOL", "TOY", "BRICK"};
	public static final String[] BRICK_TYPES = new String[] {"NORMAL", "YELLOW SOLID", "RED BREAKER", "BLUE TELEPORT", "BLACK EXPLODING"};
	
	protected AnimatedSprite fAni;
	protected AnimatedSprite fSparkle;
	protected String fType;
	protected String fItem;
	protected String fSubType;
	protected String fSubItem;
	protected int fMegaphoneOrder;

	@Override
	public void importFromWWD(String logic, String image, String animation, byte[] data) {
		fSnapToGrid = true;
		fFixed = -500;
		
		String[] sects = image.split("/");
		
		if (sects.length < 4) {
			
			if (sects[2].equals("TOYBOX")) {
				fType = "TOYBOX";
				
			}
			
			fType = "TOY";
			fItem = "SQUEAKTOY";
			
			JOptionPane.showMessageDialog(null, "The object: " + sects[2] + " isn't yet supported in Dizgruntled.\n Please email me at talk@pointcliki.com if you want this pickup added!", "Not Yet Supported", 0);
			
		} else {
				
			String type = sects[3];
			
			if (type.equals("TOOLZ")) {
				fType = "TOOL";
				fItem = sects[4];
				if (fItem.endsWith("Z") && !fItem.equals("GAUNTLETZ") && !fItem.equals("GLOVEZ") && !fItem.equals("GRAVITYBOOTZ") && !fItem.equals("WINGZ")) fItem = fItem.substring(0, fItem.length() - 1);
				
			} else if (type.equals("TOYZ")) {
				fType = "TOY";
				fItem = sects[4];
				fItem = fItem.substring(0, fItem.length() - 1);
				
			} else if (type.equals("POWERUPZ")) {
				fType = "POWERUP";
				fItem = sects[4];
				
				// Check for non powerup powerups
				if (fItem.equals("COIN")) fType = "REWARD";
				else if (fItem.equals("HEALTH1")) fType = "UTILITY";
				else if (fItem.equals("HEALTH2")) fType = "UTILITY";
				else if (fItem.equals("HEALTH3")) fType = "UTILITY";
				else if (fItem.equals("MEGAPHONEZ")) fType = "UTILITY";
				else if (fItem.equals("STOPWATCH")) fType = "UTILITY";
				else if (fItem.equals("BLACKSCREEN")) fType = "CURSE";
				else if (fItem.equals("MINICAM")) fType = "CURSE";
				else if (fItem.equals("RANDOMCOLORZ")) fType = "CURSE";
				else if (fItem.equals("SCREENSHAKE")) fType = "CURSE";
				
			} else {
				// Warp letters
				fType = "REWARD";
				fItem = sects[3];
			}
			
		}
		
		if (fItem.equals("MEGAPHONEZ")) {
			
			System.out.println(Arrays.toString(data));
			
			fMegaphoneOrder = MonolithWWD.readScore(data) - 1;
			int megaphoneItemIndex = MonolithWWD.readPoints(data);
			
			fSubType = typeForIndex(megaphoneItemIndex);
			fSubItem = itemForIndex(megaphoneItemIndex);
			
			System.out.println("Item " + fSubItem);
			
		} else {
			
			fSubType = "TOOL";
			fSubItem = "BOMB";
		
		}

		super.importFromWWD(logic, image, animation, data);
		updateAnimation();
	}
	

	@Override
	public byte[] exportToWWD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void importFromJSON(JSONObject object) {
		fSnapToGrid = true;
		fFixed = -500;
		
		super.importFromJSON(object);
		
		fType = object.optString("type");
		fItem = object.optString("item");
		fSubType = object.optString("subtype");
		fSubItem = object.optString("subitem");
		
		updateAnimation();
	}

	@Override
	public JSONObject exportToJSON() throws JSONException {
		JSONObject o = super.exportToJSON();
		o.put("type", fType);
		o.put("item", fItem);
		o.putOpt("subtype", fSubType);
		o.putOpt("subitem", fSubItem);
		return o;
	}
	
	protected void updateAnimation() {
		String image;
		if (fType.equals("TOOL")) {
			image = "TOOLZ/" + (fItem.endsWith("Z") || fItem.startsWith("WARPSTONEZ") ? fItem : fItem + "Z");
		} else if (fType.equals("TOY")) {
			image = "TOYZ/" + fItem + "Z";
		} else {
			image = "POWERUPZ/" + fItem;
		}
		if (fItem.startsWith("SECRET")) image = fItem;
		
		if (fAni != null) fAni.cleanup();
		if (fSparkle != null) fSparkle.cleanup();
		fAni = MonolithANI.fromDirectory("GAME/IMAGEZ/INGAMEICONZ/" + image);
		fSparkle = null;
		
		// Create sparkle
		String sparkle = null;
		if (fType.equals("CURSE")) sparkle = "GLITTERGREEN";
		else if (fType.equals("POWERUP")) sparkle = "GLITTERRED";
		else if (fType.equals("REWARD") && !fItem.equals("COIN")) sparkle = "GLITTERGOLD";
		if (sparkle != null) {
			fSparkle = MonolithANI.fromDirectory("GAME/IMAGEZ/" + sparkle);
			addChild(fSparkle);
		}
		
		fSpan = fAni.span();
		addChild(fAni);
		if (fMap != null) {
			fAni.start();
			if (fSparkle != null) fSparkle.start();
		}
	}
		
	public void init(Map map) {
		super.init(map);
		fAni.start();
		if (fSparkle != null) fSparkle.start();
		
		if (fItem.equals("MEGAPHONEZ")) {
		
			HumanPlayer player = levelScene().human();
		
			player.megaphoneTray().set(fMegaphoneOrder, fSubType, fSubItem);
		
		}
		
	}
	
	public String type() {
		return fType;
	}
	
	public String item() {
		return fItem;
	}
	
	public String subType() {
		return fSubType;
	}
	
	public String subItem() {
		return fSubItem;
	}
	
	@Override
	public void cleanup() {
		fAni.cleanup();
		fAni = null;
		super.cleanup();
	}

	@Override
	public String toString() {
		return "[Pickup " + fItem + "]";
	}

	@Override
	public void initProperties() {
		StringLogicProperty type = new StringLogicProperty("type") {
				
			@Override
			public String description() {
				return "The type of pickup";
			}
			
			@Override
			public String value() {
				return fType;
			}
			
			@Override
			public String[] choices() {
				return TYPES;
			}

			@Override
			public void choice(int i) {
				fType = TYPES[i];
				if (fType.equals("TOOL")) fItem = "GAUNTLETZ"; 
				else if (fType.equals("TOY")) fItem = "BEACHBALL";
				else if (fType.equals("REWARD")) fItem = "COIN";
				else if (fType.equals("POWERUP")) fItem = "GHOST";
				else if (fType.equals("CURSE")) fItem = "SCREENSHAKE";
				else if (fType.equals("UTILITY")) fItem = "MEGAPHONEZ";
				initProperties();
				updateAnimation();
			}
		};
		StringLogicProperty item = new StringLogicProperty("item") {
				
			@Override
			public String description() {
				return "The item to pickup";
			}
			
			@Override
			public String value() {
				return fItem;
			}
			
			@Override
			public String[] choices() {
				if (fType.equals("TOOL")) return TOOLS;
				else if (fType.equals("TOY")) return TOYS;
				else if (fType.equals("POWERUP")) return POWERUPS;
				else if (fType.equals("CURSE")) return CURSES;
				else if (fType.equals("REWARD")) return REWARDS;
				else return UTILITIES;
			}

			@Override
			public void choice(int i) {
				if (fType.equals("TOOL")) fItem = TOOLS[i];
				else if (fType.equals("TOY")) fItem = TOYS[i];
				else if (fType.equals("POWERUP")) fItem = POWERUPS[i];
				else if (fType.equals("CURSE")) fItem = CURSES[i];
				else if (fType.equals("REWARD")) fItem = REWARDS[i];
				else fItem = UTILITIES[i];
				initProperties();
				updateAnimation();
			}
		};
		if (fItem.equals("MEGAPHONEZ")) {
			initMegaphoneProperties(type, item);
			return;
		}
		fProperties = new LogicProperty[] {type, item};
	}
	
	public void initMegaphoneProperties( StringLogicProperty type, StringLogicProperty item ) {
		
		StringLogicProperty subtype = new StringLogicProperty("subtype") {
			
			@Override
			public String description() {
				return "The sub item of pickup";
			}
			
			@Override
			public String value() {
				return fSubType;
			}
			
			@Override
			public String[] choices() {
				return MEGAPHONE_TYPES;
			}

			@Override
			public void choice(int i) {
				fSubType = MEGAPHONE_TYPES[i];
				initProperties();
				updateAnimation();
			}
		};
		
		StringLogicProperty subitem = new StringLogicProperty("subitem") {
			
			@Override
			public String description() {
				return "The sub type of the pickup";
			}
			
			@Override
			public String value() {
				return fSubItem;
			}
			
			@Override
			public String[] choices() {
				if (fSubType.equals("TOOL")) return TOOLS;
				else if (fSubType.equals("TOY")) return TOYS;
				return BRICK_TYPES;
			}

			@Override
			public void choice(int i) {
				if (fType.equals("TOOL")) fSubItem = Grunt.TOOLS[i];
				else if (fType.equals("TOY")) fSubItem = Grunt.TOYS[i];
				else fSubItem = BRICK_TYPES[i];
				initProperties();
				updateAnimation();
			}
		};		
		fProperties = new LogicProperty[] {type, item, subtype, subitem};
		
	}
	
	public static AnimatedSprite editorIcon(JSONObject object) throws JSONException {
		String image;
		String type = object.optString("type");
		String item = object.optString("item");
		if (type.equals("TOOL")) {
			image = "TOOLZ/" + (item.endsWith("Z") || item.startsWith("WARPSTONEZ") ? item : item + "Z");
		} else if (type.equals("TOY")) {
			image = "TOYZ/" + item + "Z";
		} else {
			image = "POWERUPZ/" + item;
		}
		if (item.startsWith("SECRET")) image = item;
		
		MonolithPID pid = GruntzGame.resourceManager().pid("GAME/IMAGEZ/INGAMEICONZ/" + image + "/FRAME001");
		return new AnimatedSprite(new Image[] {pid.image()}, new Vector2f[] {pid.offset()});
	}

	@Override
	public String name() {
		return "Pickup";
	}
	
	public static String typeForIndex(int index) {
		if (index == 0) return "NONE";
		if (index < 23) return "TOOL";
		if (index < 33) return "TOY";
		if (index == 50) return "UTILITY";
		if (index < 54) return "UTILITY";
		if (index < 61) return "POWERUP";
		if (index < 75) return "CURSE";
		if (index == 75) return "UTILITY";
		if (index == 85) return "UTILITY";
		if (index < 94) return "REWARD";
		return "UTILITY";
	}
	public static String itemForIndex(int index) {
		if (index == 0) return "NONE";
		if (index < 23) return TOOLS[index - 1];
		if (index < 33) return TOYS[index - 23];
		if (index == 50) return "MEGAPHONEZ";
		if (index < 54) return UTILITIES[index - 50];
		if (index < 61) return POWERUPS[index - 54];
		if (index < 75) return CURSES[index - 61];
		if (index == 75) return "STOPWATCH";
		if (index == 80) return "COIN";
		if (index == 85) return "TOYBOX";
		if (index < 94) return REWARDS[index - 89];
		return "TIMEBOMB";
	}

}

package com.pointcliki.dizgruntled.task;

import java.util.List;

import com.pointcliki.dizgruntled.grunt.GruntState;
import com.pointcliki.dizgruntled.grunt.Task;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.utils.PathFinder;
import com.pointcliki.grid.GridCoordinate;

public class WalkToActionTask extends Task {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -8452967463204168341L;
	
	private GridCoordinate fTarget;
	private List<GridCoordinate> fPath;
	private int fPathIndex;

	public WalkToActionTask(Grunt g, GridCoordinate xy) {
		super(g);
		fTarget = xy;
	}
	
	@Override
	public boolean next() {	
		if (fGrunt.getTile().adjacent(fTarget)) fGrunt.performAction(fTarget);
		else if (fPath.size() > fPathIndex) followPath();
		else {
			fGrunt.performAction(fTarget);
		}
		return true;
	}

	protected void followPath() {
		GridCoordinate xy = fPath.get(fPathIndex);
		fPathIndex++;
		
		xy = xy.subtract(fGrunt.getTile());
		fGrunt.animate("WALK", "WALK", xy);
		fGrunt.movement().move(xy);
		fGrunt.state(GruntState.MOVING);
	}
	
	public GridCoordinate target() {
		return fTarget;
	}

	public boolean start() {
		if (fGrunt.getTile().adjacent(fTarget)) return true;
		
		PathFinder finder = new PathFinder(fGrunt.levelScene());
		fPath = finder.calculate(fGrunt.getTile(), fTarget, 1000);
		
		// Remove the last tile as we don't want to walk onto the tile
		if (fPath != null) fPath.remove(fPath.size() - 1);
		
		fPathIndex = 1;
		return fPath != null;
	}
}

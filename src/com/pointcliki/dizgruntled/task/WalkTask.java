package com.pointcliki.dizgruntled.task;

import java.util.List;

import com.pointcliki.dizgruntled.grunt.GruntState;
import com.pointcliki.dizgruntled.grunt.Task;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.utils.PathFinder;
import com.pointcliki.grid.GridCoordinate;

public class WalkTask extends Task {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -8452967463204168341L;
	
	GridCoordinate fTarget;
	protected List<GridCoordinate> fPath;
	protected int fPathIndex;

	public WalkTask(Grunt g, GridCoordinate xy) {
		super(g);
		fTarget = xy;
	}
	
	@Override
	public boolean next() {
		if (fPath.size() > fPathIndex) followPath();
		else {
			return false;
		}
		return true;
	}

	protected void followPath() {
		GridCoordinate xy = fPath.get(fPathIndex);
		fPathIndex++;
		
		xy = xy.subtract(fGrunt.getTile());
		fGrunt.animate("WALK", "WALK", xy);
		fGrunt.movement().move(xy);
		fGrunt.state(GruntState.MOVING);
	}

	public boolean start() {
		PathFinder finder = new PathFinder(fGrunt.levelScene());
		fPath = finder.calculate(fGrunt.getTile(), fTarget, 1000);
		fPathIndex = 1;
		return fPath != null;
	}
}

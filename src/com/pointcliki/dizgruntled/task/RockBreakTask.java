package com.pointcliki.dizgruntled.task;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.grunt.Task;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.logic.Hidden;

public class RockBreakTask extends Task {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3016688034160906270L;
	
	private GridCoordinate fTarget;

	public RockBreakTask(Grunt g, GridCoordinate xy) {
		super(g);
		fTarget = xy;
	}

	@Override
	public boolean next() {
		GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/GAUNTLETZGRUNT/USEGAUNTLETSWING");
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) GruntzGame.resourceManager().playSound("GRUNTZ/SOUNDZ/GAUNTLETZGRUNT/USEGAUNTLETSWING");
				return Minion.FINISH;
			}
		}, 35);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) {
					fGrunt.depleteStamina(4);
					fGrunt.map().tileToggle(fTarget);
					Hidden p = fGrunt.gridManager().getFirstEntityOfTypeAt(fTarget, Hidden.class);
					
					if (p != null) p.expose();
				}
				return Minion.FINISH;
			}
		}, 50);
		fGrunt.frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isCanceled()) fGrunt.finishAction();
				return Minion.FINISH;
			}
		}, 70);
		
		return true;
	}
	
	@Override
	public boolean start() {
		if (!fTarget.adjacent(fGrunt.getTile())) return false;
		
		return true;
	}

}

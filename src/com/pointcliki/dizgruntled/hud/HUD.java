package com.pointcliki.dizgruntled.hud;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.ui.UIEntity;

public class HUD extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 5278252715987853765L;
	
	private HudOverlay fOverlay;
	private ClayBar fClay;
	
	public HUD() {
		fOverlay = new HudOverlay(this);
		resize(new Vector2f(GruntzGame.instance().application().getWidth(), GruntzGame.instance().application().getHeight()));
		fClay = new ClayBar();
		addChild(fClay);
		
		fClickThrough = true;
	}
	
	public void showClay(int amount) {
		fClay.show(amount);
	}

	public void showText(String text) {
		fOverlay.show(text);
	}

	public void showMenu() {
		fOverlay.showMenu();
	}	
}

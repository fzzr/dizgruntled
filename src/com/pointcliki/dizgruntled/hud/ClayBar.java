
package com.pointcliki.dizgruntled.hud;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.event.ProgressEvent;
import com.pointcliki.transition.EasingLerper;
import com.pointcliki.transition.LerpMovement;
import com.pointcliki.core.EntityContainer;
import com.pointcliki.core.Sprite;
import com.pointcliki.dizgruntled.GruntzGame;

public class ClayBar extends EntityContainer {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -603068453115691323L;

	private Sprite fBack;
	private Sprite fClayAmount;
	private long fClayHide;
	private int fAmount;
	
	public ClayBar() {
		fBack = new Sprite("hud/clay");
		addChild(fBack);
		
		fClayAmount = new Sprite("hud/clay_amount") {
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 5134044335383021329L;

			@Override
			public void render(Graphics graphics, long currentTime) {
				fImage.draw(0, 192 - fAmount * 48, 20, 192, 0, 192 - fAmount * 48, 20, 192);
			}
		};
		addChild(fClayAmount.position(new Vector2f(30, 50)));
		
		position(new Vector2f(GruntzGame.instance().application().getWidth(), 0));
	}
	
	public void show(int amount) {
		fAmount = amount;
		final LerpMovement<ClayBar> lerper = new LerpMovement<ClayBar>(this, new Minion<ProgressEvent<ClayBar>>() {
			@Override
			public long run(Dispatcher<ProgressEvent<ClayBar>> dispatcher, String type, ProgressEvent<ClayBar> event) {
				return Minion.FINISH;
			}
		}, 0, 0);
		lerper.setup(200, new EasingLerper(EasingLerper.EASE_OUT));
		lerper.setup(new Vector2f(GruntzGame.instance().application().getWidth() - 83, 0));
		lerper.begin();
		
		fClayHide = timeManager().currentFrame() + 160;
		frameManager().queue(new Minion<FrameEvent>() {
			@Override
			public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (fClayHide == event.frame()) {
					lerper.setup(new Vector2f(GruntzGame.instance().application().getWidth(), 0));
					lerper.begin();
				}
				return Minion.FINISH;
			}
		}, 160);
	}
}

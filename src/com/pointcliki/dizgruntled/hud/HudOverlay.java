package com.pointcliki.dizgruntled.hud;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Sprite;
import com.pointcliki.core.TextEntity;
import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.LevelScene;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.text.HyperlinkText;
import com.pointcliki.ui.UIEntity;

public class HudOverlay extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -8367276483279381148L;
	private HUD fHUD;
	private Sprite fBack;
	private TextEntity fText;
	private HyperlinkText fContinue;
	private HyperlinkText fQuit;
	private boolean fShowingMenu = false;
	
	private static final Color COLOR = new Color(249, 216, 31);
	
	public HudOverlay(HUD hud) {
		fHUD = hud;
		
		fBack = new Sprite() {
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 23573724L;
			
			public void render(org.newdawn.slick.Graphics graphics, long currentTime) {
				graphics.setColor(new Color(0, 0, 0, 200));
				graphics.fillRect(0, 0, GruntzGame.instance().application().getWidth(), GruntzGame.instance().application().getHeight());
			};
		};
		addChild(fBack);
		
		resize(new Vector2f(GruntzGame.instance().application().getWidth(), GruntzGame.instance().application().getHeight()));
		
		fText = new TextEntity("", TextEntity.ALIGN_CENTER, GruntzGame.resourceManager().largeFont(), COLOR);
		addChild(fText.position(new Vector2f(GruntzGame.instance().application().getWidth() / 2, GruntzGame.instance().application().getHeight() / 2)));
		
		fContinue = new HyperlinkText("Continue", TextEntity.ALIGN_CENTER, GruntzGame.resourceManager().largeFont(), Color.white, COLOR) {
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = -8422268305393456577L;
			@Override
			public void click() {
				fHUD.removeChild(HudOverlay.this);
				removeChild(fContinue);
				removeChild(fQuit);
			}
		};
		fQuit = new HyperlinkText("Quit", TextEntity.ALIGN_CENTER, GruntzGame.resourceManager().largeFont(), Color.white, COLOR) {
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = -4215706932211494531L;
			@Override
			public void click() {
				GruntzGame.instance().startMenu();
			}
		};
	}

	public void show(String text) {
		fHUD.addChild(this);
				
		String[] str = text.split(" ");		
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for (String s: str) {
			i += s.length();
			if (i > 34) {
				i = s.length();
				sb.append("\n");
			}
			sb.append(s);
			sb.append(" ");
		}
		
		fText.text(sb.toString());
	}
	
	public void hide() {
		fHUD.removeChild(this);
		removeChild(fContinue);
		removeChild(fQuit);
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		super.handleUIMouseEvent(type, local, event);
		if (type.equals("mouse.click") && !fShowingMenu) {
			if (levelScene().human().won()) {
				GruntzGame.instance().startMenu();
			}
			hide();
		}
	}
	
	@Override
	public boolean greedyCapture(String type, Vector2f v, MouseEvent event) {
		return true;
	}
	
	public LevelScene levelScene() {
		return scene(LevelScene.class);
	}

	public void showMenu() {
		fShowingMenu = true;
		show("Menu");
		addChild(fContinue.position(new Vector2f(GruntzGame.instance().application().getWidth() / 2, GruntzGame.instance().application().getHeight() / 2 + 70)));
		addChild(fQuit.position(new Vector2f(GruntzGame.instance().application().getWidth() / 2, GruntzGame.instance().application().getHeight() / 2 + 110)));
	}
}

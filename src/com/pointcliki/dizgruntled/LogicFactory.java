package com.pointcliki.dizgruntled;

import org.json.JSONObject;

import com.pointcliki.dizgruntled.logic.Cause;
import com.pointcliki.dizgruntled.logic.Checkpoint;
import com.pointcliki.dizgruntled.logic.Effect;
import com.pointcliki.dizgruntled.logic.Fort;
import com.pointcliki.dizgruntled.logic.FortressFlag;
import com.pointcliki.dizgruntled.logic.Grunt;
import com.pointcliki.dizgruntled.logic.GruntCreationPoint;
import com.pointcliki.dizgruntled.logic.GruntPuddle;
import com.pointcliki.dizgruntled.logic.HelpBook;
import com.pointcliki.dizgruntled.logic.Hidden;
import com.pointcliki.dizgruntled.logic.Pickup;
import com.pointcliki.dizgruntled.logic.RollingBall;
import com.pointcliki.dizgruntled.logic.Scenery;

public class LogicFactory {

	public Logic createFromWWD(String logic, String image, String animation, byte[] data) {
		Logic l = null;
		
		// Create scenery logic
		if (logic.equals("EyeCandy") || logic.equals("EyeCandyAni") || logic.equals("BehindCandyAni") || logic.equals("DoNothing") || logic.equals("BehindCandy")) {
			l = new Scenery();
		} else if (logic.equals("GruntStartingPoint")) {
			l = new Grunt();
		} else if (logic.equals("InGameIcon")) {
			l = new Pickup();
		} else if (logic.equals("TileTrigger") || logic.equals("TileSecretTrigger") || logic.equals("SecretLevelTrigger")) {
			l = new Effect();
		} else if (logic.equals("TileTriggerSwitch")) {
			l = new Cause();
		} else if (logic.equals("CoveredPowerup") || logic.equals("Brickz")) {
			l = new Hidden();
		} else if (logic.equals("RollingBall")) {
			l = new RollingBall();
		} else if (logic.equals("ExitTrigger")) {
			l = new Fort();
		} else if (logic.equals("FortressFlag")) {
			l = new FortressFlag();
		} else if (logic.equals("CheckpointTrigger")) {
			l = new Checkpoint();
		} else if (logic.equals("InGameText")) {
			l = new HelpBook();
		} else if (logic.equals("GruntPuddle")) {
			l = new GruntPuddle();
		} else if (logic.equals("GruntCreationPoint")) {
			l = new GruntCreationPoint();
		}
		if (l == null) return null;
		try {
			l.importFromWWD(logic, image, animation, data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}
	
	public Logic createFromJSON(JSONObject object) {
		
		Class<?> c;
		try {
			c = (Class<?>) Class.forName("com.pointcliki.dizgruntled.logic." + object.optString("logic"));
		} catch (ClassNotFoundException e) {
			try {
				c = (Class<?>) Class.forName(object.optString("logic"));
			} catch (ClassNotFoundException e2) {
				System.err.println("Can't find Java class for logic: " + object.optString("logic"));
				return null;
			}
		}
		Logic l = null;
		try {
			l = (Logic) c.newInstance();
		} catch (InstantiationException e) {
			System.err.println(e.getMessage());
		} catch (IllegalAccessException e) {
			System.err.println(e.getMessage());
		}
		
		if (l == null) return null;
		l.importFromJSON(object);
		return l;
	}
}

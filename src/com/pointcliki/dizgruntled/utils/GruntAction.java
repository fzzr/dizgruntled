package com.pointcliki.dizgruntled.utils;

public enum GruntAction {
	OutOfPlay, Entering, Idle, Walking
}
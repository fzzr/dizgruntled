package com.pointcliki.dizgruntled.utils;

import java.util.HashMap;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.ImageBuffer;

import com.pointcliki.dizgruntled.GruntzGame;
import com.pointcliki.dizgruntled.rez.MonolithFile;
import com.pointcliki.dizgruntled.rez.MonolithResource;

public class GruntPalette extends Palette {
	
	//public static final float[] PRIMARY = new float[] {0.322f, 1f, 1f};
	
	//private float[] hsb = new float[3];
	
	protected HashMap<String, Color> fMapping;
	protected String fName;
	
	public GruntPalette(String name, MonolithFile f) {
		MonolithFile fx = GruntzGame.resourceManager().rez().file("GRUNTZ/PALETTEZ/GREENTOOL", "pal");
		
		fMapping = new HashMap<String, Color>();
		fName = name;
		
		byte[] data = f.data();
		byte[] data2 = fx.data();
		
		for (int i = 0; i < data.length; i += 3) {
			Color c = new Color(0, 0, 0);
			Color c2 = new Color(0, 0, 0);
			
			c.r = MonolithResource.readByte(data, i) / 255f;
			c.g = MonolithResource.readByte(data, i + 1) / 255f;
			c.b = MonolithResource.readByte(data, i + 2) / 255f;
			
			c2.r = MonolithResource.readByte(data2, i) / 255f;
			c2.g = MonolithResource.readByte(data2, i + 1) / 255f;
			c2.b = MonolithResource.readByte(data2, i + 2) / 255f;
			
			fMapping.put(c2.toString(), c);
		}
	}

	@Override
	public Color replace(Color c) {
		float ta = c.a;
		c.a = 1f;
		if (fMapping.containsKey(c.toString())) {
			Color o = fMapping.get(c.toString());
			o.a = ta;
			return o;
		}
		return c;
	}
		
	/*
	public GruntPalette(Color c) {
		java.awt.Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
	}
	
	@Override
	public Color replace(Color c) {
		
		float[] hsb2 = new float[3];
		
		java.awt.Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb2);
		if (hsb2[0] > 0.20 && hsb2[0] < 38.0 && hsb2[1] > 0.15) {

			Color rgb = new Color(java.awt.Color.HSBtoRGB(hsb2[0] - PRIMARY[0] + hsb[0], Math.max(0f, hsb2[1] - PRIMARY[1] + hsb[1]), Math.max(0f, hsb2[2] - PRIMARY[2] + hsb[2])));
			return new Color(rgb.getRed(), rgb.getGreen(), rgb.getBlue(), c.getAlpha());
			
		} else {
			return c;
		}
	}

	public Color getColor() {
		return new Color((java.awt.Color.HSBtoRGB(hsb[0], hsb[1], hsb[2])));
	}
	*/
	public String toString() {
		return fName;
	}
	
	public Image colourize(Image old) {
		if (old == null) return null;
		
		int w = old.getWidth();
		int h = old.getHeight();
		
		// Create a new image buffer to fill with pixels
		ImageBuffer buf = new ImageBuffer(w, h);
		Color c = new Color(0, 0, 0);
		
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				c = old.getColor(x, y);
				c = replace(c);
				buf.setRGBA(x, y, (int) (c.r * 255), (int) (c.g * 255), (int) (c.b * 255), (int) (c.a * 255));
			}
		return new Image(buf);
	}
}

package com.pointcliki.dizgruntled.utils;

import java.io.Serializable;

/**
 * 
 * @author Hugheth
 *
 * Marker interface
 */
public abstract class State implements Serializable, Cloneable {
	
	 /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 8346863635505810435L;

	public State clone() throws CloneNotSupportedException {
         return (State) super.clone();
	 }
}

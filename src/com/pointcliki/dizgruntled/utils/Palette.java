package com.pointcliki.dizgruntled.utils;

import org.newdawn.slick.Color;

public abstract class Palette {

	public abstract Color replace(Color c);
}

package com.pointcliki.dizgruntled.utils;

public class DirectionUtils {
	public static Direction calculateDirectionTo(Pair tile, Pair target) {
		if (tile.x() > target.x()) {
			
			if (tile.y() > target.y()) {
				
				return Direction.NorthWest;
				
			} else if (tile.y() == target.y()) {
				
				return Direction.West;
				
			} else {
				
				return Direction.SouthWest;
			}
			
		} else if (tile.x() == target.x()) {
			
			if (tile.y() > target.y()) {
				
				return Direction.North;
				
			} else if (tile.y() == target.y()) {
				
				return Direction.None;
				
			} else {
				
				return Direction.South;
			}
			
		} else {
			
			if (tile.y() > target.y()) {
				
				return Direction.NorthEast;
				
			} else if (tile.y() == target.y()) {
				
				return Direction.East;
				
			} else {
				
				return Direction.SouthEast;
			}
		}
	}
}

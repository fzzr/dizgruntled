package com.hugheth.dizgruntled.message;

import java.io.Serializable;

import com.pointcliki.dizgruntled.utils.GruntCommand;
import com.pointcliki.dizgruntled.utils.Pair;

public class CommandMessage implements Serializable {

	private static final long serialVersionUID = -5202069176948632019L;
	
	public Pair target;
	public long issueFrame;
	public long grunt;
	public GruntCommand type;

	public CommandMessage(long f, long g, GruntCommand t, Pair xy) {
		issueFrame = f;
		grunt = g;
		target = xy;
		type = t;
	}
}

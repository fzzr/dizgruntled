package com.hugheth.dizgruntled.message;

import java.io.Serializable;
import java.util.TreeMap;

import com.pointcliki.dizgruntled.utils.State;

public class HelloResponseMessage implements Serializable {

	private static final long serialVersionUID = 8292943131628704102L;
	
	public int playerID;
	public int hostID;
	
	// TODO: Add information about other players
	public int totalPlayers = 2;
	public int Players = 0;
	
	// Current world snapshot
	public TreeMap<String, State> snapshot;
	
	public HelloResponseMessage(int pid, int hid, TreeMap<String, State> s) {
		playerID = pid;
		hostID = hid;
		snapshot = s;
	}
}
